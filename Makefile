project = abc
vendor = xilinx
family = spartan6
part = xc6slx16-csg324
top_module = top

vfiles = ./verilog/top.v

include xilinx.mk

download:
	djtgcfg prog -d Nexys3 -i 0 -f $(project).bit
